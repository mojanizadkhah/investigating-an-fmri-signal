%% Estimation of behavior using FMRI
%% 
clc;
clear;
load("118_fmri_hvc_raw_new250_single");
load("118_behavior_rdm_group");
Y = RDM118_arrange_reduc;
clear RDM118_arrange_reduc

%%
% I calculated 1 - Pearson's correlation using "corrcoef" and saved the data
% into a 15*18*18 matrix called Rm.
pr = zeros(15 , 250 , 118);
Rm = zeros(15 , 118 , 118);
for i = 1:15
    temp = data(1 , i);
    pr(i , : , :) = cell2mat(temp);
    temp1(: , :) = pr(i , : , :);
    
    % Rm(i , ) is the corr matris for each person
    Rm(i , : , :) = corrcoef(temp1);
end
X = Rm;
clear i
clear pr
clear data
clear temp
clear temp1 

%% spilliting test and train
% I split the data into train and test by separating the correlation of
% 20 percent of the pictures. A 15*94*94 matrix(made of the correlation between 94 pictures) for training and a 15*24*24
% matrix(made of the correlation between 24 pictures) for validation.
% at the end of this part, I reshaped all of the data to 1 dimension for
% simplicity.
p = .8 ; % ratio train to validation
[t1 , t2 , N] = size(X);
tf = false(N,1);   
tf(1:round(p*N)) = true;
tf = tf(randperm(N)); 

X_train1 = X(: , tf,tf); 
X_train1 = reshape(X_train1,15,[]);
X_train = X_train1';

X_val1 = X(: , ~tf,~tf);
X_val1 = reshape(X_val1,15,[]);
X_val = X_val1';

Y_train1 = Y(tf,tf); 
Y_train1 = reshape(Y_train1,1,[]);
Y_train = Y_train1';

Y_val1 = Y(~tf,~tf);
Y_val1 = reshape(Y_val1,1,[]);
Y_val = Y_val1';

clear t1;
clear t2;
clear tf;
clear X_train1;
clear X_val1;
clear Y_train1;
clear Y_val1;
%% mean
% I used the mean of the correlations to predict behavior and got MSE = o.1 for validation data.
X1 = reshape(X,15,[]);
X = X1';
Y1 = reshape(Y,1,[]);
Y = Y1';
ypredmean = mean(X , 2);
mse_mean = mse(ypredmean , Y)

%% fitlm: linear regression model
% I used the linear regression to predict behavior and got MSE in order of e-6 for validation data.
mdl = fitlm(X_train,Y_train);
ypredlm = predict(mdl,X_val);
mse_reg_fitlm = mse(ypredlm , Y_val)

%% regress: multiple linear regression
% I used the multiple linear regression to predict behavior and got MSE in order of e-4 for validation data.
b = regress(Y_train,X_train);
ypredregress = b'*X_val';
mse_regress = mse(ypredregress , Y_val)

%% fitrlinear: linear regression model to high-dimensional data
% I used the fitrlinear to predict behavior and got MSE in order of e-6 for
% validation data, which is very good.
mdl = fitrlinear(X_train,Y_train);
ypredrlinear = predict(mdl,X_val);
msefitrlinear = mse(ypredrlinear , Y_val)

%% ridge: Ridge regression
% I used ridge to predict behavior and got MSE about e-7 for
% validation data, which was the best error between the models I tried, and
% that might be because the ridge model is proper for small training sets as here our training set is 15.
B = ridge(Y_train,X_train,2);
ypredridge = B'*X_val';
mse_ridge = mse(ypredridge , Y_val)
